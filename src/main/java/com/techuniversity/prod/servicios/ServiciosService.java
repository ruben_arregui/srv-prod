package com.techuniversity.prod.servicios;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.*;
import org.bson.Document;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ServiciosService {
    MongoCollection<Document> servicios;

    private static MongoCollection<Document> getServiciosCollection() {
        ConnectionString cs = new ConnectionString("mongodb://localhost:27017");
        MongoClientSettings settings = MongoClientSettings.builder()
                .applyConnectionString(cs)
                .retryWrites(true)
                .build();
        MongoClient cliente = MongoClients.create(settings);
        MongoDatabase database = cliente.getDatabase("backMayo");
        return database.getCollection("servicios");
    }
    public static List getAll() {
        MongoCollection<Document> servicios = getServiciosCollection();
        List list = new ArrayList();
        FindIterable<Document> iterDoc = servicios.find();
        Iterator iterador = iterDoc.iterator();
        while (iterador.hasNext()) {
            list.add(iterador.next());
        }
        return list;
    }
    public static void insert(String cadenaServicio) throws Exception {
        Document doc = Document.parse(cadenaServicio);
        List<Document> lstServicios = doc.getList("servicios", Document.class);
        MongoCollection<Document> servicios = getServiciosCollection();
        servicios.insertMany(lstServicios);

    }

    public static void insertBatch(String cadenaServicios) throws Exception {
        Document doc = Document.parse(cadenaServicios);
        List<Document> lstServicios = doc.getList("servicios", Document.class);
        MongoCollection<Document> servicios = getServiciosCollection();
        servicios.insertMany(lstServicios);
    }
    public static List<Document> getFiltrados(String cadenaFiltro) {
        MongoCollection<Document> servicios = getServiciosCollection();
        List lista = new ArrayList();
        Document docFiltro = Document.parse(cadenaFiltro);
        FindIterable<Document> iterDoc = servicios.find(docFiltro);
        Iterator iterador = iterDoc.iterator();
        while (iterador.hasNext()) {
            lista.add(iterador.next());
        }
        return lista;
    }
    public static List<Document> getFiltradosPeriodo(Document docFiltro) {
        MongoCollection<Document> servicios = getServiciosCollection();
        List lista = new ArrayList();
        FindIterable<Document> iterDoc = servicios.find(docFiltro);
        Iterator iterador = iterDoc.iterator();
        while (iterador.hasNext()) {
            lista.add(iterador.next());
        }
        return lista;
    }
    public static void update(String filtro, String valores) {
        MongoCollection<Document> servicios = getServiciosCollection();
        Document docFiltro = Document.parse(filtro);
        Document docValores = Document.parse(valores);
        servicios.updateOne(docFiltro, docValores);
    }
}
